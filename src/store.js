import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    categories: [],
    brands: [],
    cart: [],
  },
  getters: {
    menu(state) {
      return [
        {
          title: 'Все бренды',
          code: '',
        },
        ...state.categories,
      ];
    },
    cartCompilate(state) {
      let cart = {};
      state.cart.forEach((item) => {
        if (cart[`brand${item.brand.id}.${item.value}`]) {
          cart[`brand${item.brand.id}.${item.value}`].size += 1;
        } else {
          cart[`brand${item.brand.id}.${item.value}`] = {
            ...item,
            size: 1,
          };
        }
      });
      return cart;
    },
    cartTotal(state) {
      return state.cart.reduce((sum, item) => {
        return sum + parseFloat(item.value);
      }, 0);
    },
  },
  mutations: {
    set(state, { type, data }) {
      state[type] = data;
    },
    push(state, { type, data }) {
      state[type] = [
        data,
        ...state[type],
      ];
    },
    deleteCart(state, data) {
      let mark = true;
      state.cart = state.cart.filter((item) => {
        if (mark && ((item.brand.id === data.brand.id) && (item.value === data.value))) {
          mark = false;
          return false;
        }
        return true;
      });
    },
  },
  actions: {
    updateCategories({ commit }) {
      Vue.axios
        .get('/categories.json')
        .then((response) => { commit('set', { type: 'categories', data: response.data }); });
    },
    updateBrands({ commit }) {
      Vue.axios
        .get('/brands.json')
        .then((response) => { commit('set', { type: 'brands', data: response.data }); });
    },
    setCart({ commit }, data) {
      commit('push',
        {
          type: 'cart',
          data: data,
        });
    },
    minusBrand({ commit }, data) {
      commit('deleteCart', data);
    },
  },
});
